For each of the 4 given benchmark functions:

* implement SGA with
    * simulated binary crossover nc=2, pc=0.9
    * polynomial mutation nm=10, pm=1/D 
    * Selection mehtods:
        * (1)stochastic universal sampling
        * (2)binary tournament selection
* (3) implement GA with restricted tournament selection
    * (with the same reproductive operators as the previous SGA)
* compare 1 with 2, winner with 3
* plot best so far curve for 1, 2 and 3